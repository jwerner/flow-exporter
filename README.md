# flowexporter

This is a PHP script for bulk export of training data from flow.polar.com.

The script is forked from https://github.com/naiboo/flowexporter

It has been checked to work with the current login flow (adding CSRF token) and download links at flow.polar.com as of May 2021.

## Usage

* Edit the script _flow-export.php_
* Edit the settings at the beginning of the file
* In a console window, run the file with the PHP interpreter
  * On windows, start a Command shell
  * CD into the script directory
  * Run `php flow-export.php`
* Or, on Windows, double-click the file `start.bat`
* The export directory will be created, if it does not exist yet
* The script logs in at flow.polar.com with your credentials
* It gets a list of training records within the specified date range
* The training list is saved to a JSON file named _training_list_YYYY-MM-DD_His.json_
* It loops through the list and downloads all training TCX files
  * If zip download is configured, the zip files get extracted and removed  

## Example Output

```
C:\...\flow-exporter>php flow-export.php

Starting flow-export.php
Export directory C:\...\flow-exporter/flow-exports created
Getting ajaxLogin...done
Extract CSRF token...done
Posting login...done
Getting training/getCalendarEvents...done
Saving training list as JSON...done
3 trainings/fitness data found between 29.04.2021 and 26.05.2021
Fetching URL: https://flow.polar.com/api/export/training/tcx/NNN?compress=true...Saving file to: C:\...\flow-exporter/flow-export/YOUREMAIL-2021-04-30T08_13_15.000Z.zip...downloaded
Unzip file YOUREMAIL-2021-04-30T08_13_15.000Z.zip...done
File 1 of 3 at https://flow.polar.com/training/analysis/NNN exported
Fetching URL: https://flow.polar.com/api/export/training/tcx/MMM?compress=true...Saving file to: C:\...\flow-exporter/flow-export/YOUREMAIL-2021-05-07T08_29_02.000Z.zip...downloaded
Unzip file YOUREMAILe-2021-05-07T08_29_02.000Z.zip...done
File 2 of 3 at https://flow.polar.com/training/analysis/MMM exported
Fetching URL: https://flow.polar.com/api/export/training/tcx/MMM?compress=true...Saving file to: C:\...\flow-exporter/flow-export/YOUREMAIL-2021-05-16T11_51_25.000Z.zip...downloaded
Unzip file YOUREMAIL.de-2021-05-16T11_51_25.000Z.zip...done
File 3 of 3 at https://flow.polar.com/training/analysis/OOO exported

Finished. Duration: 3.8240339756012 sec

C:\...\flow-exporter>
```
