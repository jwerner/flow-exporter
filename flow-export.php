<?php
/**
 * Export training data from flow.polar.com as TCX files
 *
 * Date adapted: 2021-05-26
 *
 * @author https://github.com/naiboo
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 */

// Sub-directory where exports are saved. Here: relative to this script's dir:
$dir                    = __DIR__.'/flow-exports';

// Path to cookie file:
$cookiejar              = __DIR__.'/cookiejar.txt';

// Your username and password for flow.polar.com:
$_SESSION['flowlogin']  = 'YOUREMAIL@domain.tld';
$_SESSION['flowpwd']    =  'YOURPASSWORD';

// Date range to search for training records:
// Maybe this date format only works becaus e my user profile at Polar is set to german:
// $_SESSION['datefrom']   = date("d.m.Y", strtotime( '-27 days' ));
$_SESSION['datefrom']   = date('d.m.Y', strtotime('2021-01-01'));
$_SESSION['dateto']     = date("d.m.Y");

// Download TCX zip files (zip) or plain text (tcx):
$_SESSION['filetype']   = "zip";

// Unzip Zipped files after download or not? Y or N:
$_SESSION['zip']        = "Y";

// *** End of configuration ***

set_time_limit(600);
$start_time = microtime(true); 

echo "\n";
echo "Starting flow-export.php"."\n";

$tcx=$zip='';
    
// Create export dir?
if (!is_dir($dir)) {
    mkdir($dir, 0755);
    echo "Export directory ".$dir." created"."\n";
} else {
    echo "Export directory ".$dir." already exists"."\n";
}

$ch = curl_init();

echo 'Getting ajaxLogin...';

curl_setopt($ch, CURLOPT_URL,           'https://flow.polar.com/ajaxLogin');
curl_setopt($ch, CURLOPT_COOKIEJAR,     $cookiejar);
curl_setopt($ch, CURLOPT_COOKIEFILE,    $cookiejar);
curl_setopt($ch, CURLOPT_USERAGENT,     'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_REFERER, 'https://flow.polar.com/');
curl_setopt($ch, CURLOPT_AUTOREFERER, true);
// Activate this to get verbose output:
// curl_setopt($ch, CURLOPT_VERBOSE, true);

$arr = curl_exec($ch); //get login page
if($arr!==false)
    echo 'done'."\n";
else {
    echo 'Error!'."\n";
    exit(1);
}
// DEBUG echo "Result:"."\n"; echo $arr;

// Get CSRF Token
// <input type="hidden" name="csrfToken" value="3aeda..."/>
echo "Extract CSRF token...";
if (preg_match("/name=\"csrfToken\" value=\"([a-zA-z0-9-]+)\"/", $arr, $csrfToken)) {
    // Token is in $csrfToken[1];
    echo "done"."\n";
} else {
    echo "Error! Cannnot get CSRF token."."\n";
    exit(1);
}

echo 'Posting login...';
$post_fields = 'returnUrl=%2F&email=' . $_SESSION['flowlogin'] . '&password=' . $_SESSION['flowpwd'].'&csrfToken='.$csrfToken[1];

curl_setopt($ch, CURLOPT_URL, 'https://flow.polar.com/login');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);

$arr = curl_exec($ch); //post credentials
echo 'done'."\n";
// DEBUG print_r($arr);

echo 'Getting training/getCalendarEvents...';
curl_setopt($ch, CURLOPT_URL, 'https://flow.polar.com/training/getCalendarEvents?start=' . $_SESSION['datefrom'] . '&end=' . $_SESSION['dateto']);
curl_setopt($ch, CURLOPT_POST, 0);

$arr = curl_exec($ch); //get activity list
echo 'done'."\n";
// DEBUG print_r($arr);
echo 'Saving training list as JSON...';
$fp = fopen($dir.'/'.'trainings_list_'.date('Y-m-d_His').'.json', "w+");
fwrite($fp, $arr);
fclose($fp);
echo 'done'."\n";

$activity_arr = json_decode($arr);
$counter = count($activity_arr); 
// DEBUG print_r($activity_arr);

if ($counter == 0) {
    echo "No traings available, exiting";
    exit(0);
}

echo $counter." trainings/fitness data found between ".$_SESSION['datefrom']." and ".$_SESSION['dateto']."\n";

$counter=0;
// Count EXERCISE records
foreach ($activity_arr as $activity) {
    if ($activity->type == 'EXERCISE') { 
	    //echo $activity->type." ".$activity->listItemId." ".$activity->iconUrl." ".$activity->datetime."\n";
	    $counter++;
    }
}

$tz_fix_offset = '-02:00';
$count = 1;

foreach ($activity_arr as $activity) {
    if ($activity->type !== 'EXERCISE') 
        continue;

    if ( $_SESSION['filetype'] == 'tcx' ){ 
        // Export pure tcx files       
        $tcxurl = 'https://flow.polar.com/api/export/training/tcx/' . $activity->listItemId;  
        $tcxname =  $dir.'/'.$_SESSION['flowlogin'].'-'. $activity->datetime . '.tcx';	 
    }
    if ( $_SESSION['filetype'] == 'zip' ){ 	
        // Export Zipped files
        $tcxurl = 'https://flow.polar.com/api/export/training/tcx/' . $activity->listItemId . '?compress=true';   
        $tcxname = $dir.'/'.$_SESSION['flowlogin'].'-'. $activity->datetime . '.zip';
    }
    echo 'Fetching URL: ' . $tcxurl . "...";                                                                                                                                                   
    curl_setopt($ch, CURLOPT_URL, $tcxurl);
    // DEBUG print_r($tcx);

    $tcxname =  $dir.'/'.str_replace('@', '__at__', $_SESSION['flowlogin']).'-'. str_replace(':', '_', $activity->datetime) . '.zip';
    $tcxname = utf8_decode($tcxname);
    echo "Saving file to: ".$tcxname.'...';

    //Open file handler.
    $fp = @fopen($tcxname, 'w+');
    //If $fp is FALSE, something went wrong.
    if($fp === false){
        throw new Exception('Could not open: ' . $tcxname);
    }
    //Pass our file handle to cURL.
    curl_setopt($ch, CURLOPT_FILE, $fp);

    //Timeout if the file doesn't download after 20 seconds.
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);

    $fixedtcx = str_replace('000Z', '000' . $tz_fix_offset, $tcx);                                                                                                              

    //Execute the request.
    curl_exec($ch);

    //If there was an error, throw an Exception
    if(curl_errno($ch)){
        throw new Exception(curl_error($ch));
    }

    //Get the HTTP status code.
    $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    //Close the cURL handler.
    // curl_close($ch);

    //Close the file handler.
    fclose($fp);

    if($statusCode == 200){
        echo 'downloaded'."\n";
    } else{
        echo "Download failed, Status Code: " . $statusCode."\n";
    }

    // unZip flow file
    if ($_SESSION['zip'] == 'Y') {
        echo "Unzip file ".basename($tcxname).'...';
	    $zip = new ZipArchive;
     	$res = $zip->open($tcxname);
        if ($res === TRUE) {
            $zip->extractTo($dir.'/');
            $zip->close();
		}
        unlink($tcxname);
        echo 'done'."\n";
	}
	echo "File ".$count." of ".$counter." at https://flow.polar.com".$activity->url." exported\n";                                                                                                                                                                          
    $count++;
}
// close curl resource to free up system resources
curl_close($ch);     

// Delete cookie jar
unlink($cookiejar);

// End clock time in seconds
$end_time = microtime(true);
  
// Calculate script execution time
$execution_time = ($end_time - $start_time);

echo 'Finished. Duration: '.$execution_time." sec"."\n";
?>
