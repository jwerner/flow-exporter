# flowexporter Changes

## V1 - 2021-06-25

* Tidy up source code. Remove debug output. Added `start.bat`

## 2021-05-27

* Initial version after fork. Updated cURL steps to match current PolarFlow flow.
